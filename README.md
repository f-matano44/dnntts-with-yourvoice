# auto-Full-Labeling
旧名: DNNTTS-with-YourVoice

## これはなに
これは [ttslearn](https://github.com/r9y9/ttslearn) を用いた DNNTTS 学習のために必要な時間情報付きフルコンテキストラベルを Julius と pyopenjtalk を使って、コーパス (台本) と音声から生成するプログラムです。

## ダウンロード
ダウンロードは [>> こちら <<](https://gitlab.com/f-matano44/dnntts-with-yourvoice/-/releases)<br>
使い方等のドキュメントは [docs](https://gitlab.com/f-matano44/dnntts-with-yourvoice/-/tree/main/docs) にあります．
